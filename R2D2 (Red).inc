;=====WYATT J CALLISTER============================
    
    ;THIS FILE CONTAINS THE BASE PROFILE SETTING FOR THE REMOTE.
    
    ;Profiles can be easily switched by opening "Setting.inc" and commenting all
    ;off the include statements aside from "#include "p16F1789.inc"" and the
    ;configuration that the user wants
    
    ;To add a new profile:
    ;	1.	Right click on the "Profile Files" Folder and create a new ".inc"
    ;		file with the name of the new configuration
    ;	2.	Open the "Setup.inc" file inside the "Main Files" Folder
    ;	3.	Add the new ".inc" file to the #include list
    ;	4.	Comment all but the new ".inc" file and the "#include "p16F1789.inc"" 
    ;		include statments
    ;	5.	Copy and paste the contents of "Base.inc" into the new ".inc" file
    ;	6.	Modify the values inside the new ".inc" file to your pleasure.
    
;==================================================
    
;=====PROFILE SETTINGS=============================
    
    ;Profile byte bit map
    ;0-3    =	Profile Type
	;0 = DIRCT DRIVE	(AXIS VALUE IS DIRECTLY OUTPUT)
	;1 = ARCADE JOYSTICK	(AXIS MIXED WITH THE AXIS BEFORE CREATING AN ARCADE
	;			 OUTPUT FOR THE TWO CHANNELS, PREVIOS CHANNEL MUST
	;			 BE DISABLED)
	;2 = SUM AND HOLD	(AXIS IS READ A VOLOCITY)
	;3 = BINARY DRIVE	(OUTPUT IS EITHER 0x00, 0xFF, OR NO CHANGE)
	;4 = AXIS DISALED	(AXIS VALUE DOES NOT EFFECT OUTPUT)
    
    ;4-6    =	Feedback selection
	;0 = NO FEEDBACK
	;1 = FEEDBACK FROM ANALOG 1
	;2 = FEEDBACK FROM ANALOG 2
	;3 = FEEDBACK FROM ANALOG 3
	;4 = FEEDBACK FROM ANALOG 4
	    
    ;7	    =	Compliment Bit
	;0 = Don't Invert Joystick reading
	;1 = Invert Joystick reading
    
AXIS_0	    EQU	    0X00	    ;BOTTOM LEFT    (LEFT-RIGHT)
AXIS_1	    EQU	    0X00	    ;BOTTOM LEFT    (UP-DOWN)
	
AXIS_2	    EQU	    0X00	    ;TOP LEFT	    (LEFT-RIGHT
AXIS_3	    EQU	    0X00	    ;TOP LEFT	    (UP-DOWN)

AXIS_4	    EQU	    0X84	    ;BOTTOM RIGHT   (LEFT-RIGHT)
AXIS_5	    EQU	    0X01	    ;BOTTOM RIGHT   (UP-DOWN)
    
AXIS_6	    EQU	    0X00	    ;TOP RIGHT	    (LEFT-RIGHT)
AXIS_7	    EQU	    0X00	    ;TOP LEFT	    (UP-DOWN)
	    
;=====END PROFILE SETTINGS=========================
   


