;=====WYATT J CALLISTER============================
    
    ;THIS FILE CONTAINS THE MAIN SETUP FOR ALL THE FUNCTIONS IN THE PROJECT
    
	    
; PIC16F1789 Configuration Bit Settings

; ASM source line config statements

#include "p16F1789.inc"
    
;#include"Base.inc"
;#include"R2D2 (Red).inc"
;#include"R2D2 (Blue).inc"
;#include"Walle (Mini).inc"
 #include"1JS.inc"   
	    
; CONFIG1
; __config 0xC9EA
 __CONFIG _CONFIG1, _FOSC_HS & _WDTE_SWDTEN & _PWRTE_OFF & _MCLRE_ON & _CP_OFF & _CPD_OFF & _BOREN_OFF & _CLKOUTEN_OFF & _IESO_OFF & _FCMEN_OFF
; CONFIG2
; __config 0xFEFF
 __CONFIG _CONFIG2, _WRT_OFF & _VCAPEN_OFF & _PLLEN_OFF & _STVREN_ON & _BORV_LO & _LPBOR_OFF & _LVP_ON

;==================================================
 
SETUP CODE
 
SETUP
;-----INIT SERVO DATA------------------------------
 
    MOVLW	LOW (ADAVG1)	    ;SETUP START ADDRESS
    MOVWF	FSR0L
    MOVLW	HIGH (ADAVG1)
    MOVWF	FSR0H
    
    MOVLW	0X30		    ;SET ALL START UP DATA TO 
    MOVWF	TEMPREG1
ISERVO
    MOVLW	0X80
    MOVWI	FSR0++
    MOVLW	0X00
    MOVWI	FSR0++
    
    DECFSZ	TEMPREG1,1
    BRA		ISERVO
    
;-----END INIT SERVO DATA--------------------------
;-----PROFILE SETUP--------------------------------
    
;Profile byte bit map
    ;0-3    =	Profile Type
	;0 = DIRCT DRIVE
	;1 = ARCADE JOYSTICK
	;2 = SUM AND HOLD
	;3 = AXIS DISALED
    
    ;4-6    =	Feedback selection
	;0 = NO FEEDBACK
	;1 = FEEDBACK FROM ANALOG 1
	;2 = FEEDBACK FROM ANALOG 2
	;3 = FEEDBACK FROM ANALOG 3
	;4 = FEEDBACK FROM ANALOG 4
	    
    ;7	    =	Compliment Bit
	;0 = Don't Invert Joystick reading
	;1 = Invert Joystick reading
    
    CLRF	TASKBITS	    ;CLEAR ALL START UP DATA
    
    BANKSEL	EECON1	
    CLRF	EEADRH		    ;LOAD UP THE ADDRESS FOR CONFIG2
    MOVLW	0X08
    MOVWF	EEADRL		    ;/
    
    BSF		EECON1,CFGS	    ;READ FROM CONFIGRURATION
    BSF		EECON1,RD	    ;START THE READ
    NOP				    ;WAIT FOR THE PC TO JUMP AROUND
    NOP				    ;/
	
    BCF		EECON1,CFGS	    ;NO MORE READING FROM CONFIGRATION
    BCF		EECON1,RD	    ;/
    
    BCF		TASKBITS,DBUG	    ;CLEAR THE RAM DEBUG BIT
    BTFSS	EEDATH,4	    ;IS DEBUG ACTIVE?
    BSF		TASKBITS,DBUG	    ;YEP!
    
    MOVLW	LOW (PROFILE)	    ;SETUP THE POINTERS FOR PROFILE SETUP
    MOVWF	FSR0L
    MOVLW	HIGH (PROFILE)
    MOVWF	FSR0H		    ;/
    	    
    CLRF	EEADRH
    CLRF	EEADRL		    ;SETUP TO READ IN THE OFFSET DATA
    
    MOVLW	AXIS_0		    ;AXIS 0 PROFILE	2
    CALL	L_PROFILE
    
    MOVLW	AXIS_1		    ;AXIS 1 PROFILE
    CALL	L_PROFILE
    
    MOVLW	AXIS_2		    ;AXIS 2 PROFILE
    CALL	L_PROFILE
    
    MOVLW	AXIS_3		    ;AXIS 3 PROFILE
    CALL	L_PROFILE
    
    MOVLW	AXIS_4		    ;AXIS 4 PROFILE
    CALL	L_PROFILE
    
    MOVLW	AXIS_5		    ;AXIS 5 PROFILE
    CALL	L_PROFILE
    
    MOVLW	AXIS_6		    ;AXIS 6 PROFILE
    CALL	L_PROFILE
    
    MOVLW	AXIS_7		    ;AXIS 7 PROFILE
    CALL	L_PROFILE
   
;-----END PROFILE SETUP----------------------------
;-----MEMORY SETUP---------------------------------

    MOVLW	LOW (ADAVG1)	    ;LOAD THE FILE SELECT REGISTER WITH START UP VALUES
    MOVWF	FSR0L
    MOVLW	LOW (FILTER)
    MOVWF	FSR1L
    MOVLW	HIGH (ADAVG1)
    MOVWF	FSR0H
    MOVWF	FSR1H		    ;/
	    
    CLRF	TXBYTE		    ;START AT TASK 0
    MOVLW	0X01
    MOVWF	COMBYTE		    ;START AT COMMAND 1
    BSF		TASKBITS,TXADD	    ;SEND THE ADDRESS FIRST
    
    BANKSEL	AXISMETER	    ;SETUP THE METER DISPLAY
    MOVLW	0X03
    MOVWF	AXISMETER	    ;/
    
    MOVLW	0X20		    ;SETUP TO READ THE STATUS ALWAYS
    MOVWF	TRBITS
    MOVWF	RCBITS		    ;/
    
    MOVLW	0X12		    ;START WITH FULL BRIGHTNESS, AND NORMAL DISPALY
    MOVWF	DISSEL		    ;/
    
    MOVLW	0XFF		    ;SET START UP VALUES
    MOVWF	DIGIO
    MOVWF	PDSTO
    
    CLRF	FCOUNT		    
    CLRF	SONGSEL
    CLRF	SETSEND		    ;/
   
;-----END MEMORY SETUP-----------------------------
;-----PORT SETUP-----------------------------------
    
    BANKSEL	ANSELA	    ;ANALOG SETTINGS (HOW DOES THIS WORK BILL??)
    MOVLW	0X2F
    MOVWF	ANSELA
    CLRF	ANSELB
    CLRF	ANSELC
    CLRF	ANSELD
    MOVLW	0X07
    MOVWF	ANSELE	    ;/
    
    BANKSEL	LATA	    ;CLEAR OUT RANDOM START UP DATA
    CLRF	LATA
    CLRF	LATB
    CLRF	LATC
    CLRF	LATD	   
    CLRF	LATE	    ;/
    
    BANKSEL	TRISA	    ;SET THE TRIS BITS UP
    MOVLW	0XFF
    MOVWF	TRISA	    ;SET AS INPUTS FOR THE ANALOG CONVERTIONS AND TO
    MOVWF	TRISB	    ;SAVE ON POWER CONSUMPTION
    MOVWF	TRISE	    ;/
    
    MOVLW	0XBF	    
    MOVWF	TRISC	    ;SET PORT,6 AS AN OUTPUT FOR USART
    CLRF	TRISD	    ;SET PORTD AS ALL OUTPUTS FOR THE LEDs
    
;-----END PORT SETUP-------------------------------
;-----TIMER 2 SETUP--------------------------------
    
    BANKSEL	PR2		    ;SETUP TIMER TO TO OVERFLOW EVERY 20mS
    MOVLW	D'250'		    ;THIS WILL KEEP DATA FROM BEING TRANSMITTED
    MOVWF	PR2		    ;FASTER THEN THE SERVOS CAN UPDATE
    
    BANKSEL	T2CON
    MOVLW	0X27		    ;0x27
    MOVWF	T2CON	
    
;-----END TIMER 2 SETUP----------------------------
;-----A/D SETUP------------------------------------
    
    BANKSEL	ADCON1
    MOVLW	0X60	    ;FOSC/32, VREF- = VSS, VREG+ = VDD,
    MOVWF	ADCON1
    
    MOVLW	0X0F	    ;DISABLE AUTO TRIGGER, SINGLE ENDED CONVERTION
    MOVWF	ADCON2
    
    MOVLW	0X80
    MOVWF	ADCON0
    
 ;-----TEST CALIBRATION-----------------------------
    ;This section test if all the right buttons are pressed, signaling calibration
    
    BANKSEL	PORTB		    ;TEST IF ALL THE RIGHT BUTTONS ARE PRESSED
    MOVLW	0X0F
    ANDWF	PORTB,0
    BTFSC	STATUS,Z	    ;/
    GOTO	CALIBRATE
    
    BANKSEL	EEDATL		    ;SETUP TO LOAD THE JOYSTICK OFFSETS
    CLRF	EEADRH
    MOVLW	0XFF
    MOVWF	EEADRL		    ;/
    BSF		EECON1,RD
    INCF	EEDATL,0	    ;TEST IF THE OFFSETS HAVE BEEN PROGRAMED ALREADY
    BTFSC	STATUS,Z	    ;/
    GOTO	CALIBRATE	    ;IF NOT CALIBRATE THE JOYSTICKS
    
    BANKSEL	TRISB
    MOVLW	0XFF
    MOVWF	TRISB
    
    BANKSEL	ADCON0
    
;-----END TEST CALIBRATION-------------------------
    
    BSF		ADCON0,ADON ;GET THE FIRST CONVERTION STARTED
    BSF		ADCON0,1    ;/ (GO/DONE BIT)
    
;-----END A/D SETUP--------------------------------
;-----USART SETUP----------------------------------
    BANKSEL	SP1BRGH
    CLRF	SP1BRGH
    MOVLW	0XCF
    MOVWF	SP1BRGL
    
    BANKSEL	TX1STA
    BSF         TX1STA, BRGH
    BSF         BAUD1CON,BRG16
    BCF         TX1STA, SYNC
    BSF         RC1STA, SPEN
    BSF         TX1STA, TXEN
    BSF         RC1STA, CREN	    ;DON'T RECEIVE
    
;-----END USART SETUP------------------------------
;-----INTERRUPT SETUP------------------------------
    
    BANKSEL	PIE1
    CLRF	PIE1
    BSF		PIE1,RCIE
    BSF		PIE1,TMR2IE
    
    CLRF	PIE2
    CLRF	PIE3
    CLRF	PIE4
    
    BANKSEL	INTCON
    BSF		INTCON,PEIE
    BSF		INTCON,GIE
    
;-----END INTERRTUP SETUP--------------------------
    
    RETURN
;=====END SETUP====================================
    
    LIST


